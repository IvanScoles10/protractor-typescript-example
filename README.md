# Typescript Protractor Ejemplo


## Estructura del proyecto

Dentro de la carpeta specs se encuentran todos los tests los de que van a correr con protractor y los que no, todos archivos .ts (typescript)

Ejemplo

En la carpeta specs pueden crear mas carpetas y organizar su proyecto, por ejemplo yo puse dentro de la carpeta spec, una carpeta db / connectDB.ts

specs/db/connectDB.specs.ts (todos los archivos que tengan el formato *.spec.ts) van a ser tomados en cuenta como test por lo tanto van a ser ejecutados como tests por protractor

## Setup:
* Para correrlo es necesario tener instalado node [Node](http://nodejs.org) (v6.x.x or later)
* Protractor [here](http://www.protractortest.org/#/tutorial#setup)
* Instalar las dependencias del proyecto `npm install` dentro del proyecto
* En una pestaña separada tener corriendo el webdriver `webdriver-manager start`.

## Correr los tests:
* `npm run test`

## Problemas
* Correr `node -v` y verificar que la version de node 6.x.x es igual o superior a esta
* Correr `java -version` y verificar que tenemos el JDK instalado
* Correr `npm ls -g --depth=0` y estar seguro protractor@5.1.2 de que esta instalado global NPM package.
* Estar seguro de que tenemos corriendo selenium `webdriver-manager start`.
* Verificar de que tenemos selenium actualizado `webdriver-manager update`. 
* Borrar la carpeta `./dist` para que los test esten frescos
