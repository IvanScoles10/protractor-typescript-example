"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var conf_1 = require("./conf");
// Modify browsers used for testing
conf_1.config.capabilities = undefined;
conf_1.config.multiCapabilities = [
    { 'browserName': 'chrome' },
    { 'browserName': 'firefox' }
];
// Use modifed base config as protractor config
exports.config = conf_1.config;
//# sourceMappingURL=conf.crossBrowsers.js.map