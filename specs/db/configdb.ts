import * as oracledb from 'oracledb';

export default oracledb.IConnectionAttributes = {
    user: 'user',
    password: 'tiger',
    connectString: 'URL'
}