// we don't need protractor in this case
// import { browser } from 'protractor';
import * as oracledb from "oracledb";
import configdb from "./configdb";

class ConnectDAO {
  	public async ConnectionDB(): Promise<oracledb.IConnection> {
    	return await oracledb.getConnection(configdb);
  	}
  	public fetchData(
    	connection: oracledb.IConnection
  	): oracledb.IPromise<oracledb.IExecuteReturn> {
    	return connection.execute("SELECT CURRENT_TIMESTAMP FROM dual");
  	}
}

describe("Test connection", () => {
  	it("Should connect correctly to oracle db", async () => {
		let connectDao = new ConnectDAO();
		try {
			const connection = await connectDao.ConnectionDB();
			const results = await connectDao.fetchData(connection);
			console.log(`results : ${JSON.stringify(results, null, 4)}`)
		} catch (err) {
			console.log(`error caught ${err}`);
		}
	});
});
